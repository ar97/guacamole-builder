# I am not keeping this repo updated anymore.

# Guacamole builder playbook for Debian GNU/Linux 10 (Buster)

# What you can do with these playbooks :

- You can install guacamole
- You can set up guacamole with SQL. (Connections cannot be bulk-added as it requires a lot of table updates. As of now you will have to add connections manually through the UI. Right now I had to go with user-mapping.xml.)
- You can se up guacamole and use user-mapping.xml for authentication.

# Coming - To Do

- Add RDP support
- USE LDAP for authentication
- Automate LDAP user addition and connection creation.

# PART 1 - Authentication using user-mapping.xml - Follow if you want the basic stuff to be up and running.

**This guide assumes that you have a working Debian and Ansible installation.**

## Clone this repository

```
git clone https://gitlab.hopbox.in/abhiram/guacamole-builder.git
```

> This playbook downloads, configures, installs the Gucamole proxy daemon, and installs all its dependencies. It also installs the web application that Apache has released. It is served using tomcat.


## Run the playbook

> Make sure that your machine is connected to the internet as the plabook downloads required files for its installation. Become root :

```
sudo -s
```


```
ansible-playbook playbooks/guacamole-builder-auth_xml.yml
```

> You can add -v flag to increase the verbosity. Number of v's is proportional to the amount of log.

> If everything went well, you will now be able to access the web application running on port 8080.

**Go to http://localhost:8080/guacamole**

Don't forget to use the username 'guacamole_user' and password 'password' (Without the quotes, of course).

# How it works (Each task)

- Makes a temporary directory in which all required files will be downloaded into.
- Clones a git repository containing modified source files, web application and the playbook.
- Installs all dependencies for making the server.
- Copies the web application into required destination.
- Does autoconfiguration for making the server.
- Makes the server.
- Installs the server.
- Runs ldconfigure so that the new service is ready to run.
- Makes directories for extensions and libraries. (Not required right now but will be useful)
- Copies guacamole configuration file and base authentication configuration into its destinations.
- Links guacmole configuration file with tomcat.
- Restarts tomcat9.service.
- Restarts guacd.service.

> If the playbok exited with error; it will most likely be when the playbook tried to start the guacamole proxy daemon. You might have to do the server installation part By hand once more. If you get this error then :

- Go to the directory where the server source files lie.
 
```
cd /tmp/guacamole-builder/guacamole-server-1.0.0/
```

- Run the following commands.
> BECOME ROOT. If a sudo user do:

```
sudo -s
```

```
autoreconf -fi
```

```
./configure --with-init-dir=/etc/init.d
```

```
make && make install
```

```
ldconf
```

Do one of the following  based on your choice

```
systemctl start guacd.service && systemctl start tomcat9.service
```

```
/etc/init.d/guacd restart && /etc/init.d/tomcat9 restart
```

> Check whether both daemons are running or not.

```
systemctl status guacd.service && systemctl status tomcat9.service
```

# PART 2 - Authentication using MySQL - MySQL database backend for user authentication


# Part A of 2

**This guide assumes that you have a working Debian and Ansible installation.**

## Clone this repository

```
git clone https://gitlab.hopbox.in/abhiram/guacamole-builder.git
```

> This playbook downloads, configures, installs the Gucamole proxy daemon, and installs all its dependencies. It also installs the web application that Apache has released. It is served using tomcat. This method uses MySQL backend for authenticating users and managing connections.


## Run the plabook

> Make sure that your machine is connected to the internet as the plabook downloads required files for its installation.

```
ansible-playbook playbooks/guacamole-builder-auth_MySQL-1.yml
```

> You can add -v flag to increase the verbosity. Number of v's is proportional to the amount of logs.

> If everything went well, you will see a message telling you to follow the part B of this guide.


# Part B

> The first playbook will have downloaded all files required and installed all dependencies. Follow the rest of the instructions to set up mysql authentication for your Guacamole installation.

Now that MySQL is installed (MariaDB), you have to configure it. Follow the steps.

- Become root

```
sudo -s
```

- Run the secure installation playbook that MariaDB provides and set a password for the root user.

```
MYSQL_SECURE_INSTALLATION
```

- Provide the required details when prompted. Set a new root user password. The password that is going to work with the second playbook is 'password'. If you want to, you can either make changes in the playbooks, or you can just change the root password after a succesful execution of the playbook. Guacamole will not  use the root user, and it will only use 'guacamole_user' that will get automatically added when the playbook runs.


- Navigate to the directory where the playbook lies and do :

```
ansible-playbook guacamole-builder-auth_MySQL-2.yml
```

After doing so you will have everything up and running.

If something is not right try and do it manually. Check the following things are running the systemd way or the initd way, whichever you are using.

- Status of mysql service ( MariaDB service ) and check if the shema is correctly imported.
- Status and log of tomcat.
- Status of guacd.
- Nothing much could go wrong. Still look into the individual steps and see if any one has gone wrong or even have not yet happened.

# ADDING CONNECTIONS - Works with user-mapping.xml ONLY.

```
ansible-playbook add-connections.yml
```

> As of now, if you need to update the connections, please go into the user-mapping.xml file and delete the <connections> tag and update the list inside the playbook with new connections that you want to add, and then run the playbook again.
